# Домашнее задание к занятию Troubleshooting

### Цель задания

Устранить неисправности при деплое приложения.

### Чеклист готовности к домашнему заданию

1. Кластер K8s.

### Задание. При деплое приложение web-consumer не может подключиться к auth-db. Необходимо это исправить

1. Установить приложение по команде:

```shell
kubectl apply -f https://raw.githubusercontent.com/netology-code/kuber-homeworks/main/3.5/files/task.yaml
```

2. Выявить проблему и описать.
3. Исправить проблему, описать, что сделано.
4. Продемонстрировать, что проблема решена.

## Ответ

![Alt text](https://github.com/wineperm/SHDEVOPS-2/assets/15356046/dc802885-311b-48a1-9206-ab815d492a49)

![Alt text](https://github.com/wineperm/SHDEVOPS-2/assets/15356046/e7b8f390-ebfe-4cd8-bb5c-f37168d4d2ec)

![Alt text](https://github.com/wineperm/SHDEVOPS-2/assets/15356046/cb24c97e-1ef2-43a4-ab65-c5641f914bad)

В команде curl в контейнере busybox не указан адрес сервиса auth-db, поэтому curl будет пытаться обращаться к доменному имени auth-db вне кластера Kubernetes, вместо того, чтобы обращаться к сервису auth-db внутри кластера.

Чтобы исправить эту проблему, необходимо указать адрес сервиса auth-db в команде curl. Это можно сделать, добавив имя сервиса и пространство имен в команду следующим образом:

```
command:
- sh
- -c
- while true; do curl auth-db.data:80; sleep 5; done
```

В этом случае curl будет обращаться к сервису auth-db в пространстве имен data на порту 80.

[task.yaml](https://github.com/wineperm/SHDEVOPS-2/blob/main/kuber-homeworks/3.5/task.yaml)

![Alt text](https://github.com/wineperm/SHDEVOPS-2/assets/15356046/24699aea-9997-4b71-a82f-a0ce40060adf)

### Правила приёма работы

1. Домашняя работа оформляется в своём Git-репозитории в файле README.md. Выполненное домашнее задание пришлите ссылкой на .md-файл в вашем репозитории.
2. Файл README.md должен содержать скриншоты вывода необходимых команд, а также скриншоты результатов.
3. Репозиторий должен содержать тексты манифестов или ссылки на них в файле README.md.
