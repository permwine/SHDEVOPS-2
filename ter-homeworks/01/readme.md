# Домашнее задание к занятию «Введение в Terraform»

### Цели задания

1. Установить и настроить Terrafrom.
2. Научиться использовать готовый код.

------

### Чек-лист готовности к домашнему заданию

1. Скачайте и установите **Terraform** версии =1.5.Х (версия 1.6 может вызывать проблемы с Яндекс провайдером) . Приложите скриншот вывода команды ```terraform --version```.
2. Скачайте на свой ПК этот git-репозиторий. Исходный код для выполнения задания расположен в директории **01/src**.
3. Убедитесь, что в вашей ОС установлен docker.
4. Зарегистрируйте аккаунт на сайте https://hub.docker.com/, выполните команду docker login и введите логин, пароль.

------

### Инструменты и дополнительные материалы, которые пригодятся для выполнения задания

1. Репозиторий с ссылкой на зеркало для установки и настройки Terraform: [ссылка](https://github.com/netology-code/devops-materials).
2. Установка docker: [ссылка](https://docs.docker.com/engine/install/ubuntu/). 
------
### Внимание!! Обязательно предоставляем на проверку получившийся код в виде ссылки на ваш github-репозиторий!
------

### Задание 1

1. Перейдите в каталог [**src**](https://github.com/netology-code/ter-homeworks/tree/main/01/src). Скачайте все необходимые зависимости, использованные в проекте. 
2. Изучите файл **.gitignore**. В каком terraform-файле, согласно этому .gitignore, допустимо сохранить личную, секретную информацию?
3. Выполните код проекта. Найдите  в state-файле секретное содержимое созданного ресурса **random_password**, пришлите в качестве ответа конкретный ключ и его значение.
4. Раскомментируйте блок кода, примерно расположенный на строчках 29–42 файла **main.tf**.
Выполните команду ```terraform validate```. Объясните, в чём заключаются намеренно допущенные ошибки. Исправьте их.
5. Выполните код. В качестве ответа приложите: исправленный фрагмент кода и вывод команды ```docker ps```.
6. Замените имя docker-контейнера в блоке кода на ```hello_world```. Не перепутайте имя контейнера и имя образа. Мы всё ещё продолжаем использовать name = "nginx:latest". Выполните команду ```terraform apply -auto-approve```.
Объясните своими словами, в чём может быть опасность применения ключа  ```-auto-approve```. Догадайтесь или нагуглите зачем может пригодиться данный ключ? В качестве ответа дополнительно приложите вывод команды ```docker ps```.
8. Уничтожьте созданные ресурсы с помощью **terraform**. Убедитесь, что все ресурсы удалены. Приложите содержимое файла **terraform.tfstate**. 
9. Объясните, почему при этом не был удалён docker-образ **nginx:latest**. Ответ **обязательно** подкрепите строчкой из документации [**terraform провайдера docker**](https://docs.comcloud.xyz/providers/kreuzwerker/docker/latest/docs).  (ищите в классификаторе resource docker_image )

## Ответ

- /.terraform/* значит что весь каталог /terraform со всеми подкаталогами будет игнорироваться.
- .terraform* значит что все файлы в каталоге .terraform будут игнорироваться.
- !.terraformrc значит что файл .terraformrc в текущем каталоге будет игнорироваться.
- *.tfstate значит что все файлы с расширением tfstate будут игнорироваться.
- .tfstate. значит что все файлы с расширением tfstate в каталоге .terraform будут игнорироваться.
- personal.auto.tfvars значит, что файл personal.auto.tfvars будет игнорироваться.
- ```храним тут personal.auto.tfvars```

- ![Alt text](https://github.com/wineperm/SHDEVOPS-2/assets/15356046/45e45ab1-1e04-49be-be2c-6a59bf1a4c2c)
- "result": "b4V87vhqtqD1CuNo",

- resource "docker_image" "nginx" {  # Нет второго блока, (uniq_name) уникального названия, задается самостоятельно, распространяется на текущий проект.
- resource "docker_container" "nginx" {  # Имя может начинаться с буквы или подчеркивания _ и может содержать только буквы, цифры, подчеркивания и тире.
- name  = "example_${random_password.random_string.result}" # Строка должна содержать ресурс из классификатора (type) random_password ("random_password" строка 15), имя random_string ("random_string" строка 15) и аргумент "result" cгенерированный случайным образом в зависимости от заданных параметров (строки от 16 до 20).
- ![Alt text](https://github.com/wineperm/SHDEVOPS-2/assets/15356046/8204fb7e-1ee2-4d85-9b0d-8e0774ead7d1)

- ![Alt text](https://github.com/wineperm/SHDEVOPS-2/assets/15356046/bd498c48-1592-43fb-8fc4-d565df7275ef)
```
terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 3.0.1"
    }
  }
  required_version = ">=0.13" /*Многострочный комментарий.
 Требуемая версия terraform */
}
provider "docker" {}

#однострочный комментарий

resource "random_password" "random_string" {
  length      = 16
  special     = false
  min_upper   = 1
  min_lower   = 1
  min_numeric = 1
}


resource "docker_image" "nginx" {
  name         = "nginx:latest"
  keep_locally = true
}

resource "docker_container" "nginx" {
  image = docker_image.nginx.image_id
  name  = "example_${random_password.random_string.result}"

  ports {
    internal = 80
    external = 8000
  }
}
```
- ![Alt text](https://github.com/wineperm/SHDEVOPS-2/assets/15356046/f7e6f60a-c616-48e6-ab6c-d495913e5dfa)
- Опасность в том, что если применить авто-одобрение, то эти изменения будут автоматически приняты и добавлены в сборку. Это может привести к неожиданному изменению кода и возможному нарушению работы приложения. Не стоит использовать в ответственных местах. Удобно использовать при обучении, локальном тестировании сборок.

- ![Alt text](https://github.com/wineperm/SHDEVOPS-2/assets/15356046/16daa6aa-4531-4c5e-8fa4-c87067845f23)

- Образ не удаляется потому что в конфигурационном файле задан аргумент: keep_locally – true, если его сменить на false, то при уничтожении ресурсов с помощью terraform образы тоже удалятся.
- keep_locally (Boolean) If true, then the Docker image won't be deleted on destroy operation. If this is false, it will delete the image from the docker local storage on destroy operation.   


------

## Дополнительное задание (со звёздочкой*)

**Настоятельно рекомендуем выполнять все задания со звёздочкой.** Они помогут глубже разобраться в материале.   
Задания со звёздочкой дополнительные, не обязательные к выполнению и никак не повлияют на получение вами зачёта по этому домашнему заданию. 

### Задание 2*

1. Создайте в облаке ВМ. Сделайте это через web-консоль, чтобы не слить по незнанию токен от облака в github(это тема следующей лекции). Если хотите - попробуйте сделать это через terraform, прочита документацию yandex cloud. Используйте файл ```personal.auto.tfvars``` и гитигнор или иной, безопасный способ передачи токена!
2. Подключитесь к ВМ по ssh и установите стек docker.
3. Найдите в документации docker provider способ настроить подключение terraform на вашей рабочей станции к remote docker context вашей ВМ через ssh.
4. Используя terraform и  remote docker context, скачайте и запустите на вашей ВМ контейнер ```mysql:8``` на порту ```127.0.0.1:3306```, передайте ENV-переменные. Сгенерируйте разные пароли через random_password и передайте их в контейнер, используя интерполяцию из примера с nginx.(```name  = "example_${random_password.random_string.result}"```  , двойные кавычки и фигурные скобки обязательны!) 
```
    environment:
      - "MYSQL_ROOT_PASSWORD=${...}"
      - MYSQL_DATABASE=wordpress
      - MYSQL_USER=wordpress
      - "MYSQL_PASSWORD=${...}"
      - MYSQL_ROOT_HOST="%"
```

6. Зайдите на вашу ВМ , подключитесь к контейнеру и проверьте наличие секретных env-переменных с помощью команды ```env```. Запишите ваш финальный код в репозиторий.

## Ответ

- ![Alt text](https://github.com/wineperm/SHDEVOPS-2/assets/15356046/78fb88cd-da02-4089-956e-4eefb6e95864)

```
terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.2"
    }
  }
}

provider "docker" {
  host = "ssh://qwerty@158.160.51.49:22"
  ssh_opts = ["-o", "StrictHostKeyChecking=no", "-o", "UserKnownHostsFile=/dev/null"]
}

# Pulls the image
resource "docker_image" "mysql" {
  name = "mysql:8"
  keep_locally = false
}

# Create a container
resource "docker_container" "task2" {
  image = docker_image.mysql.image_id
  name  = "task2"
  env = ["MYSQL_DATABASE=wordpress", "MYSQL_USER=wordpress", "MYSQL_ROOT_HOST=%", "MYSQL_ROOT_PASSWORD=${random_password.random_string.result}", "MYSQL_PASSWORD=${random_password.random_string.result}"]
}

resource "random_password" "random_string" {
  length      = 16
  special     = false
  min_upper   = 1
  min_lower   = 1
  min_numeric = 1
}

```

------

### Правила приёма работы

Домашняя работа оформляется в отдельном GitHub-репозитории в файле README.md.   
Выполненное домашнее задание пришлите ссылкой на .md-файл в вашем репозитории.

### Критерии оценки

Зачёт ставится, если:

* выполнены все задания,
* ответы даны в развёрнутой форме,
* приложены соответствующие скриншоты и файлы проекта,
* в выполненных заданиях нет противоречий и нарушения логики.

На доработку работу отправят, если:

* задание выполнено частично или не выполнено вообще,
* в логике выполнения заданий есть противоречия и существенные недостатки. 

